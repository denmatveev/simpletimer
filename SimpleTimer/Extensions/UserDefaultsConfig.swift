//
//  UserDefaultsConfig.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 10/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import Foundation

struct UserDefaultsConfig {
    @UserDefault("start_time", defaultValue: 120)
    static var startTime: Int
    
    @UserDefault("always_on_top", defaultValue: true)
    static var alwaysOnTop: Bool
    
    @UserDefault("sound_is_on", defaultValue: true)
    static var soundIsOn: Bool
}
