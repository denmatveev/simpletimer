//
//  ControlsView.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 04/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct ControlsView: View {
    @EnvironmentObject var timerData: TimerModel
    
    var body: some View {
        VStack {
            ButtonView(iconName: timerData.soundIsOn ? "sound-on" : "sound-off")
                .frame(width: 20, height: 20)
                .onTapGesture {
                    self.timerData.soundIsOn.toggle()
                }
            ButtonView(iconName: timerData.timerIsOn ? "pause" : "play")
                .frame(width: 32, height: 32)
                .onTapGesture {
                    self.timerData.togglePlay()
                }
            ButtonView(iconName: "stop")
                .frame(width: 20, height: 20)
                .onTapGesture {
                    self.timerData.stop()
                }
        }
    }
}

struct ControlsView_Previews: PreviewProvider {    
    static var previews: some View {
        ControlsView().environmentObject(TimerModel(startTimeInSeconds: 120))
    }
}
