//
//  TimerView.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 03/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct TimerView: View {
    @Binding var currentTime: Int
    
    var body: some View {
        GeometryReader { geometry in
            Text(verbatim: self.formatTime(self.currentTime))
                .font(Font.monospacedDigit(.system(size: self.textSize(containerSize: geometry.size)))())
                .minimumScaleFactor(0.5)
        }
    }
    
    // Picks the font size depending on parent's dimensions
    private func textSize(containerSize: CGSize) -> CGFloat {
        let scale: CGFloat = 0.8
        return containerSize.height > containerSize.width ?
            containerSize.width * scale : containerSize.height * scale
    }
    
    // Formats seconds into MM:SS string
    func formatTime(_ timeInSeconds: Int) -> String {
        let mins: Int = timeInSeconds / 60
        let secs: Int = timeInSeconds % 60
        return String(format: "%02d:%02d", arguments: [mins, secs])
    }
}

struct TimerView_Previews: PreviewProvider {
    static var previews: some View {
        TimerView(currentTime: .constant(120))
    }
}
