//
//  progressIndicatorView.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 08/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct ProgressIndicatorView: View {
    @EnvironmentObject var timerData: TimerModel
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                Spacer()
                Rectangle()
                    .foregroundColor(Color.init("alert"))
                    .frame(height: geometry.size.height * (1 - CGFloat(self.timerData.currentTime) / CGFloat(self.timerData.startTime)))
                    .animation(.linear(duration: 1))
            }
        }
    }
}

struct progressIndicatorView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressIndicatorView().environmentObject(TimerModel(startTimeInSeconds: 120))
    }
}
