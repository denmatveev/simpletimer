//
//  ContentView.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 03/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI
import AudioToolbox

struct ContentView: View {
    @EnvironmentObject var timerData: TimerModel
    
    @State var isHovered = false
    
    var body: some View {
        
        HStack(spacing: 0) {
            Spacer()
            ZStack {
                TimerView(currentTime: $timerData.currentTime)
                    .onHover(perform: { isEntered in
                        withAnimation(.easeIn(duration: 0.2)) {
                            self.isHovered = isEntered
                        }
                    })
    
                if isHovered {
                    UpDownControlView().environmentObject(timerData)
                }
            }
            ControlsView().environmentObject(timerData)
        }
        .padding()
        .background(ProgressIndicatorView().environmentObject(timerData))
        .onReceive(timerData.$currentTime) { time in
            if time == self.timerData.firstAlertTime {
                self.playAlertSound(soundName: "alert2", fileExtension: "wav")
            } else if time == 0 {
               self.playAlertSound(soundName: "alert1", fileExtension: "wav")
            }
        }
    }
}

extension ContentView {
    private func playAlertSound(soundName: String, fileExtension: String) {
        guard self.timerData.soundIsOn else {
            return
        }
        //AudioServicesPlayAlertSound(kSystemSoundID_UserPreferredAlert)
        if let soundURL = Bundle.main.url(forResource: soundName, withExtension: fileExtension) {
            var mySound: SystemSoundID = 0
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &mySound)
            // Play
            AudioServicesPlaySystemSound(mySound)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(TimerModel(startTimeInSeconds: 120))
    }
}
