//
//  UpDownControlView.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 08/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct UpDownControlView: View {
    @EnvironmentObject var timerData: TimerModel
    
    var body: some View {
        VStack(alignment: .trailing) {
            ChevronIcon(direction: .up)
                .gesture(TapGesture().modifiers(.option).onEnded {
                    self.timerData.increaseStartTime(by: .small)
                })
                .onTapGesture {
                    self.timerData.increaseStartTime(by: .normal)
                }
            
            Spacer()
            
            ChevronIcon(direction: .down)
                .gesture(TapGesture().modifiers(.option).onEnded {
                    self.timerData.decreaseStartTime(by: .small)
                })
                .onTapGesture {
                    self.timerData.decreaseStartTime(by: .normal)
                }
        }
        .frame(maxHeight: .infinity)
        
    }
    
    struct ChevronIcon: View {
        
        var direction: ChevronDirection
        
        @State private var isHovered = false
                
        var body: some View {
            Image(direction == .up ? "chevron-up" : "chevron-down")
                .renderingMode(.template)
                .resizable()
                .aspectRatio(1.0, contentMode: .fit)
                .frame(height: 20)
                .frame(maxWidth: .infinity)
                .background(Color.primary.opacity(isHovered ? 0.3 : 0.1))
                .clipShape(RoundedRectangle(cornerRadius: 4))
                .onHover(perform: { isEntered in
                    withAnimation(.easeIn(duration: 0.2)) {
                        self.isHovered = isEntered
                    }
                })
        }
    }
    
    enum ChevronDirection{
        case up
        case down
    }
}


struct UpDownControlView_Previews: PreviewProvider {
    static var previews: some View {
        UpDownControlView().environmentObject(TimerModel(startTimeInSeconds: 120))
    }
}
