//
//  ButtonView.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 05/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import SwiftUI

struct ButtonView: View {
    @Environment(\.colorScheme) var colorScheme // To detect dark or light mode
    
    var iconName: String
    
    @State var isHovered = false
    
    var body: some View {
        Image(iconName)
            .renderingMode(.template) // Ability to change icon color. Adapts to the light mode automatically as well
            .resizable()
            .aspectRatio(1.0, contentMode: .fit)
            .opacity(isHovered ? 1 : 0.85)
            .onHover(perform: { isEntered in
                withAnimation(.easeIn(duration: 0.2)) {
                    self.isHovered = isEntered
                }
            })
    }
    
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView(iconName: "play")
    }
}
