//
//  TimerModel.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 05/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import Foundation
import Combine

class TimerModel: ObservableObject {
    @Published var currentTime: Int = 0
    @Published private(set) var timerIsOn = false {
        willSet(newValue){
            if newValue == true && currentTime == 0 {
                reset()
            }
        }
    }
    @Published var soundIsOn = true
    
    var startTime: Int
    var firstAlertTime: Int = 15
    
    private let timerPublisher = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    private var cancellableTimer: AnyCancellable?
    
    init(startTimeInSeconds: Int) {
        self.startTime = startTimeInSeconds
        reset()
        
        cancellableTimer = timerPublisher
            .sink() { value in
                if self.timerIsOn {
                    if self.currentTime > 0 {
                        self.currentTime -= 1
                    } else {
                        self.timerIsOn = false
                    }
                }
            }
    }
    
    func stop() {
        timerIsOn = false
        reset()
    }
    
    func togglePlay() {
        timerIsOn = !timerIsOn
    }
    
    func reset() {
        currentTime = startTime
    }
    
    func increaseStartTime(by delta: TimeDelta) {
        if startTime + delta.rawValue <= 3600 {
            startTime += delta.rawValue
            reset()
        }
    }
    
    func decreaseStartTime(by delta: TimeDelta) {
        if startTime - delta.rawValue > 0 {
            startTime -= delta.rawValue
            reset()
        }
    }
}

enum TimeDelta: Int {
    case minimum = 1
    case small = 10
    case normal = 30
    case big = 60
}
