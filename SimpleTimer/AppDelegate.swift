//
//  AppDelegate.swift
//  SimpleTimer
//
//  Created by Денис Матвеев on 03/04/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import Cocoa
import SwiftUI
import Combine

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var window: NSWindow!

    @IBOutlet weak var lightModeMenuItem: NSMenuItem!
    @IBOutlet weak var darkModeMenuItem: NSMenuItem!
    @IBOutlet weak var systemModeMenuItem: NSMenuItem!
    @IBOutlet weak var alwaysOnTopMenuItem: NSMenuItem!
    
    @Published var alwaysOnTop = UserDefaultsConfig.alwaysOnTop
    let timerData = TimerModel(startTimeInSeconds: UserDefaultsConfig.startTime)
    
    let customModePublisher = NSApp.publisher(for: \.appearance)
        .map { appearance -> SchemeMode in
            if let name = appearance?.name {
                switch name {
                case .aqua: return .light
                case .darkAqua: return .dark
                default: return .system
                }
            } else {
                return .system
            }
        }
        .eraseToAnyPublisher()
    
    var cancellableMode: AnyCancellable?
    var cancellableOnTop: AnyCancellable?

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        // Create the SwiftUI view that provides the window contents.
        let contentView = ContentView().environmentObject(timerData)
        
        // Reading from user defaults
        timerData.soundIsOn = UserDefaultsConfig.soundIsOn

        // Create the window and set the content view. 
        window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable],
            backing: .buffered, defer: false)
        window.center()
        window.setFrameAutosaveName("Main Window") // Remembers the window's size and position
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
        
        // Checking system and custom appearance modes
        cancellableMode = customModePublisher
            .receive(on: RunLoop.main)
            .sink() { mode in
                self.lightModeMenuItem.state = mode == .light ? .on : .off
                self.darkModeMenuItem.state = mode == .dark ? .on : .off
                self.systemModeMenuItem.state = mode == .system ? .on : .off
            }
        
        // Checking always on top option
        cancellableOnTop = $alwaysOnTop
            .receive(on: RunLoop.main)
            .sink() { isOnTop in
                if isOnTop {
                    self.window.level = .floating
                    self.alwaysOnTopMenuItem.state = .on
                } else {
                    self.window.level = .normal
                    self.alwaysOnTopMenuItem.state = .off
                }
            }
    }
    
    // Closing the app if the window is closed
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        
        // Saving user defaults
        UserDefaultsConfig.startTime = timerData.startTime
        UserDefaultsConfig.alwaysOnTop = alwaysOnTop
        UserDefaultsConfig.soundIsOn = timerData.soundIsOn
    }
    
    @IBAction func togglePlay(_ sender: NSMenuItem) {
        timerData.togglePlay()
    }
    
    @IBAction func stopTimer(_ sender: NSMenuItem) {
        timerData.stop()
    }
    
    @IBAction func toggleSound(_ sender: NSMenuItem) {
        timerData.soundIsOn.toggle()
    }
    
    @IBAction func selectMode(_ sender: NSMenuItem) {
        
        switch sender.tag {
        case 1:
            NSApp.appearance = NSAppearance(named: .aqua)
        case 2:
            NSApp.appearance = NSAppearance(named: .darkAqua)
        case 3:
            NSApp.appearance = nil
        default:
            break
        }
    }
    
    @IBAction func selectTimeChange(_ sender: NSMenuItem) {
        switch sender.tag {
        case 4:
            timerData.increaseStartTime(by: .normal)
        case 5:
            timerData.increaseStartTime(by: .small)
        case 6:
            timerData.decreaseStartTime(by: .small)
        case 7:
            timerData.decreaseStartTime(by: .normal)
        default:
            break
        }
    }
    
    @IBAction func toggleAlwaysOnTop(_ sender: NSMenuItem) {
        alwaysOnTop.toggle()
    }
    
    enum SchemeMode {
        case light
        case dark
        case system
    }
    
    
}

